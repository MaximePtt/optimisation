# Optimisation pour la gestion
## Projet MIN MAKESPAN
### Execution
A la racine du projet :
```
java -cp build/class Main
```
__OU__
<br>
_Directement via l'interface IntelliJ :_ `Run 'Main'`
### Build
A la racine du projet :
```
javac src/*.java -d build/class -encoding utf8
```
<br>
Projet réalisé par Maxime Potet et Alexis Vatin
