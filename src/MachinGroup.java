import java.util.ArrayList;

public class MachinGroup {
    ArrayList<Machin> machins;

    public MachinGroup(){
        machins = new ArrayList<>();
    }

    public void addMachin(Machin machin){
        machins.add(machin);
    }

    public Machin getShorterMachin(){
        int shorterMachinIndice = 0;
        for(int i = 0 ; i < machins.size() ; i++){
            int currentMachinDuration = machins.get(i).getFullDuration();
            int shorterMachinDuration = machins.get(shorterMachinIndice).getFullDuration();
            if(currentMachinDuration < shorterMachinDuration) shorterMachinIndice = i;
        }
        return machins.get(shorterMachinIndice);
    }

    public Machin getLongerMachin(){
        int longerMachinIndice = 0;
        for(int i = 0 ; i < machins.size() ; i++){
            int currentMachinDuration = machins.get(i).getFullDuration();
            int shorterMachinDuration = machins.get(longerMachinIndice).getFullDuration();
            if(currentMachinDuration > shorterMachinDuration) longerMachinIndice = i;
        }
        return machins.get(longerMachinIndice);
    }

    public Machin getRandomMachin(){
        final int min = 0;
        final int max = this.machins.size() - 1;
        int randomMachinIndice = min + (int)(Math.random() * (max - min + 1));
        return machins.get(randomMachinIndice);
    }

    @Override
    public String toString() {
        return machins.toString();
    }
}
