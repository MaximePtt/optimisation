import java.util.ArrayList;

public class Machin {
    String name;
    ArrayList<Task> tasks;

    public Machin(String name){
        this.name = name;
        this.tasks = new ArrayList<>();
    }

    public void addTask(Task newTask){
        this.tasks.add(newTask);
    }

    public int getFullDuration(){
        return tasks.stream().mapToInt(task -> task.duration).sum();
    }

    @Override
    public String toString() {
        return name + ": " + tasks;
    }
}
