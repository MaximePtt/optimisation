import java.util.ArrayList;

public class Println {
    public static void printResultsImOrI2m(Instance instance) throws Exception{
        System.out.println("Im : " + instance.toString());
        System.out.println("");
        System.out.println("Borne inférieure \"maximum\" = " + instance.getBorneInfMax());
        System.out.println("Borne inférieure \"moyenne\" = " + instance.getBorneInfMoy());
        System.out.println("");
        MachinGroup machinGroupLSA = Algorithms.LSA(instance);
        System.out.println("Résultat LSA = " + machinGroupLSA.getLongerMachin().getFullDuration());
        System.out.println("ratio LSA = " + Algorithms.ratioLSA(instance.getBorneInfMax(), instance.getBorneInfMoy(), machinGroupLSA.getLongerMachin().getFullDuration()));
        System.out.println("");
        MachinGroup machinGroupLPT = Algorithms.LPT(instance);
        System.out.println("Résultat LPT = " + machinGroupLPT.getLongerMachin().getFullDuration());
        System.out.println("ratio LPT = " + Algorithms.ratioLPT(instance.getBorneInfMax(), instance.getBorneInfMoy(), machinGroupLPT.getLongerMachin().getFullDuration()));
        System.out.println("");
        MachinGroup machinGroupRMA = Algorithms.RMA(instance);
        System.out.println("Résultat RMA = " + machinGroupRMA.getLongerMachin().getFullDuration());
        System.out.println("ratio RMA = " + Algorithms.ratioRMA(instance.getBorneInfMax(), instance.getBorneInfMoy(), machinGroupRMA.getLongerMachin().getFullDuration()));
    }

    public static void printResultsIr(ArrayList<Instance> instancesList) throws Exception{
        double ratioMoyLSA = 0.0;
        double ratioMoyLPT = 0.0;
        double ratioMoyRMA = 0.0;

        System.out.print("Ir : ");
        for(int i = 0 ; i < instancesList.size() ; i++) instancesList.get(i).toString();

        System.out.println("\n");
        for(int i = 0 ; i < instancesList.size() ; i++){
            Instance instance = instancesList.get(i);
            MachinGroup machinGroupLSA = Algorithms.LSA(instance);
            MachinGroup machinGroupLPT = Algorithms.LPT(instance);
            MachinGroup machinGroupRMA = Algorithms.RMA(instance);
            ratioMoyLSA += Algorithms.ratioLSA(instance.getBorneInfMax(), instance.getBorneInfMoy(), machinGroupLSA.getLongerMachin().getFullDuration());
            ratioMoyLPT += Algorithms.ratioLPT(instance.getBorneInfMax(), instance.getBorneInfMoy(), machinGroupLPT.getLongerMachin().getFullDuration());
            ratioMoyRMA += Algorithms.ratioRMA(instance.getBorneInfMax(), instance.getBorneInfMoy(), machinGroupRMA.getLongerMachin().getFullDuration());
        }
        ratioMoyLSA /= instancesList.size();
        ratioMoyLPT /= instancesList.size();
        ratioMoyRMA /= instancesList.size();
        System.out.println("ratio moyen LSA = " + ratioMoyLSA);
        System.out.println("ratio moyen LPT = " + ratioMoyLPT);
        System.out.println("ratio moyen RMA = " + ratioMoyRMA);
    }
}
