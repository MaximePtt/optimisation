import java.util.ArrayList;

public class Instance {
    ArrayList<Task> tasks;
    int nbMachins;

    public Instance(int nbMachins){
        tasks = new ArrayList<>();
        this.nbMachins = nbMachins;
    }

    public void addTask(Task task){
        tasks.add(task);
    }

    public Task getTask(int i){
        return tasks.get(i);
    }

    public int getBorneInfMax(){
        int bornInfMax = 0;
        for(int i = 0 ; i < tasks.size() ; i++){
            Task currentTask = tasks.get(i);
            if(currentTask.duration > bornInfMax) bornInfMax = currentTask.duration;
        }
        return bornInfMax;
    }

    public int getBorneInfMoy(){
        int delaysSum = 0;
        for(int i = 0 ; i < tasks.size() ; i++) delaysSum += tasks.get(i).duration;
        return delaysSum / nbMachins;
    }

    @Override
    public String toString() {
        return "(" + nbMachins + "M;" + tasks.toString() + ")";
    }
}
