import java.util.ArrayList;

public class Instances {
    public static Instance getIm(int nbMachines){
        Instance instance = new Instance(nbMachines);
        for(int i = 1 ; i <= nbMachines*nbMachines; i++){
            instance.addTask(new Task(1));
        }
        instance.addTask(new Task(nbMachines));
        return instance;
    }

    public static Instance getI2m(int nbMachines){
        Instance instance = new Instance(nbMachines);
        for(int i = 1 ; i <= 3 ; i++) instance.addTask(new Task(nbMachines));
        for(int i = 1 ; i <= nbMachines-1 ; i++){
            instance.addTask(new Task(nbMachines + i));
            instance.addTask(new Task(nbMachines + i));
        }
        return instance;
    }

    public static ArrayList<Instance> getIrList(int nbMachins, int nbTasks, int nbInstances, int delayMin, int delayMax){
        ArrayList<Instance> instancesList = new ArrayList<>();
        for(int i = 0 ; i < nbInstances ; i++){
            instancesList.add(new Instance(nbMachins));
            for(int j = 0 ; j < nbTasks ; j++){
                int currentTaskDelay = delayMin + (int)(Math.random() * (delayMax - delayMin + 1));
                instancesList.get(i).addTask(new Task(currentTaskDelay));
            }
        }
        return instancesList;
    }
}
