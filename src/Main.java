import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        int nbMachines = 3;
        Instance instance = new Instance(nbMachines);
        instance.addTask(new Task(3));
        instance.addTask(new Task(1));
        instance.addTask(new Task(1));
        instance.addTask(new Task(2));
        instance.addTask(new Task(3));

        System.out.println("Example :");
        System.out.println("");
        System.out.println(instance);
        System.out.println("");
        System.out.println("LSA : " + Algorithms.LSA(instance));
        System.out.println("");
        System.out.println("LPT : " + Algorithms.LPT(instance));
        System.out.println("");
        System.out.println("RMA : " + Algorithms.RMA(instance));
        System.out.println("");
        System.out.println("");

        int response1;
        int response2;
        System.out.println("Menu :");
        System.out.println("");
        System.out.println("1: Instance Im");
        System.out.println("2: Instance I'm");
        System.out.println("3: Instance Ir");
        System.out.println("");
        System.out.print("Entrez une réponse : ");
        response1 = scanInt();
        while(response1 < 1 || response1 > 3 ){
            System.out.print("La réponse doit être '1', '2' ou '3', Réessayez : ");
            response1 = scanInt();
        }
        switch(response1){
            case 1:{
                System.out.print("Entrez le nombre de machines : ");
                response2 = scanInt();
                while(response2 < 1){
                    System.out.print("Le nombre de machine doit être un entier supérieur à 0, Réessayez : ");
                    response2 = scanInt();
                }
                Instance instanceIm = Instances.getIm(response2);
                Println.printResultsImOrI2m(instanceIm);
                break;
            }
            case 2:{
                System.out.print("Entrez le nombre de machines : ");
                response2 = scanInt();
                while(response2 < 1){
                    System.out.print("Le nombre de machine doit être un entier supérieur à 0, Réessayez : ");
                    response2 = scanInt();
                }
                Instance instanceI2m = Instances.getI2m(response2);
                Println.printResultsImOrI2m(instanceI2m);
                break;
            }
            case 3:{
                int responseNbMachines;
                int responseNbTasks;
                int responseNbInstances;
                int responseDelayMin;
                int responseDelayMax;

                System.out.print("Entrez le nombre de machines : ");
                response2 = scanInt();
                while(response2 < 1){
                    System.out.print("Le nombre de machine doit être un entier supérieur à 0, Réessayez : ");
                    response2 = scanInt();
                }
                responseNbMachines = response2;

                System.out.print("Entrez le nombre de tâches : ");
                response2 = scanInt();
                while(response2 < 1){
                    System.out.print("Le nombre de tâches doit être un entier supérieur à 0, Réessayez : ");
                    response2 = scanInt();
                }
                responseNbTasks = response2;

                System.out.print("Entrez le nombre d'instances : ");
                response2 = scanInt();
                while(response2 < 1){
                    System.out.print("Le nombre d'instances doit être un entier supérieur à 0, Réessayez : ");
                    response2 = scanInt();
                }
                responseNbInstances = response2;

                System.out.print("Entrez la durée minimum : ");
                response2 = scanInt();
                while(response2 < 1){
                    System.out.print("La durée minimum doit être un entier supérieur à 0, Réessayez : ");
                    response2 = scanInt();
                }
                responseDelayMin = response2;

                System.out.print("Entrez la durée maximum : ");
                response2 = scanInt();
                while(response2 < responseDelayMin){
                    System.out.print("Ici, la durée maximum doit être un entier supérieur ou égal à " + responseDelayMin + ", Réessayez : ");
                    response2 = scanInt();
                }
                responseDelayMax = response2;

                Println.printResultsIr(Instances.getIrList(responseNbMachines, responseNbTasks, responseNbInstances, responseDelayMin, responseDelayMax));
                break;
            }
        }
    }

    private static int scanInt(){
        Scanner scanner = new Scanner(System.in);
        String response = scanner.next();
        int responseInteger = 0;
        try{
            responseInteger = Integer.parseInt(response);
        }
        catch(Exception e){
            responseInteger = 0;
        }
        return responseInteger;
    }
}
