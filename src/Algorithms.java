public class Algorithms {
    public static MachinGroup LSA(Instance instance) throws Exception {
        if (instance.nbMachins > 0) {
            // Création des n machines
            MachinGroup machins = new MachinGroup();
            for (int i = 0; i < instance.nbMachins; i++) machins.addMachin(new Machin("m" + i));

            // On ajoute chaque tâche une par une aux machines
            for (int i = 0; i < instance.tasks.size(); i++) machins.getShorterMachin().addTask(instance.getTask(i));

            return machins;
        }
        else{
            
            throw new Exception("Le nombre de machines doit être supérieur à 0.");
        }
    }

    public static MachinGroup LPT(Instance instance) throws Exception {
        Instance clonedInstance = new Instance(instance.nbMachins);
        for(Task task : instance.tasks) clonedInstance.addTask(task.clone());
        clonedInstance.tasks.sort(null);
        MachinGroup machins = LSA(clonedInstance);
        return machins;
    }

    public static MachinGroup RMA(Instance instance) throws Exception {
        if (instance.nbMachins > 0) {
            MachinGroup machins = new MachinGroup();

            // Création des n machines
            for (int i = 0; i < instance.nbMachins; i++) machins.addMachin(new Machin("m" + i));

            // On ajoute chaque tâche une par une aux machines
            for (int i = 0; i < instance.tasks.size(); i++) machins.getRandomMachin().addTask(instance.getTask(i));

            return machins;
        }
        else{
            throw new Exception("Le nombre de machines doit être supérieur à 0.");
        }
    }

    public static double ratioLSA(int bornInfMax, int bornInfMoy, int resultLSA){
        double B = Math.max(bornInfMax, bornInfMoy);
        double ratioLSA = resultLSA / B;
        return ratioLSA;
    }

    public static double ratioLPT(int bornInfMax, int bornInfMoy, int resultLPT){
        double B = Math.max(bornInfMax, bornInfMoy);
        double ratioLPT = resultLPT / B;
        return ratioLPT;
    }

    public static double ratioRMA(int bornInfMax, int bornInfMoy, int resultRMA){
        double B = Math.max(bornInfMax, bornInfMoy);
        double ratioRMA = resultRMA / B;
        return ratioRMA;
    }
}