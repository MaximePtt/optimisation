public class Task implements Comparable<Task>,Cloneable{
    int duration;

    public Task(int duration){
        this.duration = duration;
    }

    @Override
    protected Task clone() throws CloneNotSupportedException {
        return (Task) super.clone();
    }

    @Override
    public String toString() {
        return "" + duration;
    }

    @Override
    public int compareTo(Task comparedTask) {
        return comparedTask.duration - this.duration; // Decroissant
    }
}
